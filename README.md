# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### Objectives ###

  * Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
  * Extend a tiny web server in Python, to check understanding of basic web architecture
  * Use automated tests to check progress (plus manual tests for good measure)

### What do I need?  Where will it work? ###

* Designed for Unix, mostly interoperable on Linux (Ubuntu) or MacOS. May also work on Windows, but no promises. A Linux virtual machine may work, but our experience has not been good; you may want to test on shared server ix-dev.

* You will need Python version 3.4 or higher. 

* Designed to work in "user mode" (unprivileged), therefore using a port number above 1000 (rather than port 80 that a privileged web server would use)

* Windows 10 note: The new Windows bash on ubuntu looks promising. If you are running Windows 10, please give this a try and let me know if the Ubuntu/bash environment is suitable for CIS 322 develpment. 

### Description ###

The purpose of this project is to edit preexisting code in order to make a (small) basic python webserver operational for learning. The following changes were made specifically to pageserver/pageserver.py.

  a) If URL ends with `name.html` or `name.css` (i.e., if `path/to/name.html` is in document path (from DOCROOT)), send the contents of `name.html` or `name.css` with the proper http reponse.

  b) If `name.html` is not in the current directory, respond with 404 (not found).

  c) If a page starts with one of the following symbols (~, //, ..), respond with 403 (forbidden error). For example, `url=localhost:5000/..name.html` or `~name.html` would result in a 403 (forbidden error).

  
### Testing 
To test, start by getting the server up an running with the steps provided.

1. `git clone <yourGitRepository> <target Directory>`

2. `cd <targetdirectory>`

3. `make run or make start`

4. Test it with a browser while your server is running in a background process. 

5. `make stop`
  
Alternatively, use the script under "tests" folder to test the expected outcomes in an automated fashion. It is accompanied by README file and comments (inside tests.sh) explaining how to test your code.

# Contributors 
--------------

Ronny Fuentes <ronnyf@uoregon.edu>

